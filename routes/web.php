<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::get('/', function() {
	return view('home');
});

Route::get('/home', function() {
	return view('home');
})->name('home');

Route::get('/user', function() {
	return view('home');
})->name('user');
Route::get('user/create', function() {
	return view('home');
});
Route::get('user/{id}/edit', function() {
	return view('home');
})->name('user.id.edit');
Route::get('/json/user/timezone-select', 'UserController@timezoneSelect');
Route::resource('json/user', 'UserController');

Route::get('/blog', function() {
	return view('home');
})->name('blog');
Route::get('blog/create', function() {
	return view('home');
});
Route::get('blog/{slug}', function() {
	return view('home');
})->name('blog.slug');
Route::resource('json/blog', 'BlogController');
