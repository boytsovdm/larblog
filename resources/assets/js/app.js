require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import swal from 'sweetalert';

import App from './App.vue';
import User from './components/User.vue';
import UserCreate from './components/UserCreate.vue';
import UserEdit from './components/UserEdit.vue';
import Blog from './components/Blog.vue';
import BlogCreate from './components/BlogCreate.vue';
import BlogShow from './components/BlogShow.vue';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

let isLogged = document.head.querySelector("[name=is-logged]").content === '1';

let routes = [];

if (isLogged) {
	routes = [
		{
			name: 'User',
			path: '/user',
			component: User
		},
		{
			name: 'UserCreate',
			path: '/user/create',
			component: UserCreate
		},
		{
			name: 'UserEdit',
			path: '/user/:id/edit',
			component: UserEdit
		},
		{
			name: 'Blog',
			path: '/',
			component: Blog
		},
		{
			name: 'Blog',
			path: '/home',
			component: Blog
		},
		{
			name: 'BlogCreate',
			path: '/blog/create',
			component: BlogCreate
		},
		{
			name: 'BlogShow',
			path: '/blog/:id',
			component: BlogShow
		},
	];
} else {
	routes = [
		{
			name: 'Blog',
			path: '/',
			component: Blog
		},
		{
			name: 'Blog',
			path: '/home',
			component: Blog
		},
		{
			name: 'BlogShow',
			path: '/blog/:id',
			component: BlogShow
		},
	];
}

const router = new VueRouter({
	mode: 'history',
	routes: routes,
	logged: isLogged
});

new Vue( Vue.util.extend({ router }, App) )
    .$mount('#larblog_container');

