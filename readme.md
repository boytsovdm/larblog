## Configuration steps

1. Run:
~~~
git clone https://bitbucket.org/boytsovdm/larblog.git larblog
cd larblog
~~~
2. Setup your database connection and APP_KEY in .env.example file and save it as .env
3. Check you have latest stable versions of nodejs and npm
4. Then run:
~~~
composer install
php artisan migrate
php artisan db:seed --class=BlogsTableDataSeeder
npm install
npm run production
~~~
