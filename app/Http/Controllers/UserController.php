<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Camroncade\Timezone\Timezone;

class UserController extends RegisterController
{
	public function __construct()
	{
	}

	public function register(Request $request)
	{
		$this->validator($request->all())->validate();
		event(new Registered($user = $this->create($request->all())));
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    return response()->json(User::all());
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param array $data
	 * @return User|\Illuminate\Http\Response
	 */
    public function create(array $data)
    {
	    return parent::create($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    return response()->json($this->register($request));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    return response()->json(User::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    return response()->json(User::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = User::find($id);
	    $user->first_name = $request->get('first_name');
	    $user->last_name = $request->get('last_name');
	    $user->timezone = $request->get('timezone');
	    $user->save();

	    return response()->json('Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $user = User::find($id);
	    $user->delete();

	    return response()->json('Successfully Deleted');
    }

	/**
	 * Get Timezone Select form
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function timezoneSelect()
	{
		$timezone = new Timezone;
		$timezones = [];
		foreach ($timezone->timezoneList as $key => $value) {
			$timezones[] = [
				'text' => $key,
				'value' => $value,
			];
		}
		return response()->json($timezones);
	}
}
