<?php

use Illuminate\Database\Seeder;

class BlogsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('blogs')->insert([
		    'title' => 'Example blog 0',
		    'slug' => 'example-blog-0',
		    'content' => '<p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus convallis auctor interdum. Ut elit elit, egestas at condimentum in, bibendum quis purus. Proin tellus eros, condimentum non quam non, ornare tempor ligula. Curabitur ut pretium mauris. Donec ac magna ac sapien facilisis placerat. Phasellus et lorem sed nisi tincidunt congue. In pharetra mi metus, pellentesque maximus ante bibendum vel. Duis eu magna id est euismod imperdiet. Ut in libero facilisis, tempus nisl vel, luctus odio. Nulla condimentum lectus ipsum, eu lacinia sem euismod eu. Maecenas nulla erat, pretium non commodo eu, sagittis nec orci. Donec ut nunc mi. Donec blandit turpis felis, eu convallis tellus malesuada sit amet. Aliquam semper leo lorem, id accumsan quam vehicula id. Phasellus tempus, erat at eleifend tempus, tellus eros molestie mi, ut posuere mi purus at quam. Suspendisse mattis purus augue, eget blandit turpis volutpat ut. In quis ante porta, interdum turpis eget, auctor massa. Aliquam elementum laoreet nulla. Cras ex massa, elementum non interdum ac, bibendum quis massa. Mauris ultricies urna sit amet efficitur bibendum. Donec vel nisi ante. Donec libero velit, dictum nec odio ac, maximus consequat mi. Proin dapibus eros eget nunc egestas scelerisque.</p>',
		    'created_at' => date('Y-m-d H:i:s'),
		    'updated_at' => date('Y-m-d H:i:s'),
	    ]);

	    DB::table('blogs')->insert([
		    'title' => 'Example blog 1',
		    'slug' => 'example-blog-1',
		    'content' => '<p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus convallis auctor interdum. Ut elit elit, egestas at condimentum in, bibendum quis purus. Proin tellus eros, condimentum non quam non, ornare tempor ligula. Curabitur ut pretium mauris. Donec ac magna ac sapien facilisis placerat. Phasellus et lorem sed nisi tincidunt congue. In pharetra mi metus, pellentesque maximus ante bibendum vel. Duis eu magna id est euismod imperdiet. Ut in libero facilisis, tempus nisl vel, luctus odio.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Nulla condimentum lectus ipsum, eu lacinia sem euismod eu. Maecenas nulla erat, pretium non commodo eu, sagittis nec orci. Donec ut nunc mi. Donec blandit turpis felis, eu convallis tellus malesuada sit amet. Aliquam semper leo lorem, id accumsan quam vehicula id. Phasellus tempus, erat at eleifend tempus, tellus eros molestie mi, ut posuere mi purus at quam. Suspendisse mattis purus augue, eget blandit turpis volutpat ut. In quis ante porta, interdum turpis eget, auctor massa. Aliquam elementum laoreet nulla. Cras ex massa, elementum non interdum ac, bibendum quis massa. Mauris ultricies urna sit amet efficitur bibendum. Donec vel nisi ante. Donec libero velit, dictum nec odio ac, maximus consequat mi. Proin dapibus eros eget nunc egestas scelerisque.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Ut quis enim pretium metus blandit dictum. Sed fermentum diam at ultrices cursus. Aliquam facilisis lorem ornare nunc consectetur, at aliquet ex tincidunt. Nulla consectetur metus ullamcorper sapien malesuada, vel laoreet orci pretium. Sed non nisl sit amet elit dapibus aliquet in ut est. Nam pretium nibh at magna convallis posuere. Donec at gravida mi. Nulla viverra mattis erat vitae rutrum. Integer et laoreet massa, id feugiat massa. Nunc ullamcorper, nisi luctus porta mollis, ligula dui elementum dui, laoreet venenatis lectus odio non metus. Nullam ut est dui. Quisque at mauris porttitor, vestibulum quam a, pharetra urna.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">In elementum pharetra lorem, et volutpat dui semper a. Cras non purus sed urna dignissim ultricies vitae gravida leo. Nunc eget mi pharetra, viverra purus dapibus, volutpat dui. Curabitur non nibh non augue venenatis eleifend. Etiam in tristique velit. Donec at massa et nunc ultrices porta. Sed sagittis, lorem id venenatis pellentesque, magna mi aliquam eros, sit amet gravida justo est id tortor. Mauris convallis est nec justo ullamcorper vestibulum. Phasellus non tortor sodales diam ultrices semper. Curabitur tristique, augue non porta placerat, dui dolor laoreet magna, ut aliquet diam massa quis nisi. Maecenas nec purus vel nisl euismod gravida at a nibh. Nulla quis augue varius risus fringilla suscipit vel vitae mi. Ut rutrum risus luctus laoreet fringilla. Vestibulum dictum nulla diam, sed pulvinar turpis feugiat at. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Cras vitae tortor libero. Duis imperdiet eget risus et scelerisque. In pellentesque augue viverra nunc porttitor, a lacinia tellus tempus. Aliquam quam odio, egestas at nisl vitae, ultricies egestas purus. Nam pretium laoreet semper. Donec finibus, massa nec pharetra sollicitudin, quam lectus dignissim velit, vitae tincidunt dolor ante et justo. Praesent lobortis, risus vitae scelerisque imperdiet, purus mauris feugiat urna, vel placerat lorem purus sit amet quam. Praesent pellentesque finibus quam, tincidunt blandit odio auctor blandit. Vivamus mollis risus sed faucibus tristique. Fusce efficitur vehicula maximus. In eget laoreet magna. Nam ut libero eu eros tempus sollicitudin. Nulla eget eros ut dolor sagittis consectetur id sit amet justo. Proin tincidunt lorem velit, a mattis turpis tempus ac. Donec aliquam in diam eget pretium. Donec commodo aliquet diam eget porttitor.</p>',
		    'created_at' => date('Y-m-d H:i:s'),
		    'updated_at' => date('Y-m-d H:i:s'),
	    ]);

	    DB::table('blogs')->insert([
		    'title' => 'Example blog 2',
		    'slug' => 'example-blog-2',
		    'content' => '<p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;"><b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus convallis auctor interdum. Ut elit elit, egestas at condimentum in, bibendum quis purus. Proin tellus eros, condimentum non quam non, ornare tempor ligula. Curabitur ut pretium mauris. Donec ac magna ac sapien facilisis placerat. Phasellus et lorem sed nisi tincidunt congue. In pharetra mi metus, pellentesque maximus ante bibendum vel. Duis eu magna id est euismod imperdiet. Ut in libero facilisis, tempus nisl vel, luctus odio.</b></p>',
		    'created_at' => date('Y-m-d H:i:s'),
		    'updated_at' => date('Y-m-d H:i:s'),
	    ]);
    }
}
